-- Create the database
CREATE DATABASE IF NOT EXISTS property_management;
USE property_management;

-- Table buildings
CREATE TABLE IF NOT EXISTS buildings
(
    id          INT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    date_construction DATE NULL,
    address      VARCHAR(100) NOT NULL
);

-- Table apartment_types
CREATE TABLE IF NOT EXISTS apartment_types
(
    id   INT PRIMARY KEY,
    type VARCHAR(10) NOT NULL
);

-- Table apartments
CREATE TABLE IF NOT EXISTS apartments
(
    id               INT PRIMARY KEY,
    building_id      INT,
    number           INT NOT NULL,
    area             INT,
    apartment_type_id INT,
    max_occupants    INT,
    FOREIGN KEY (building_id) REFERENCES buildings (id),
    FOREIGN KEY (apartment_type_id) REFERENCES apartment_types (id)
);

-- Table appliances
CREATE TABLE IF NOT EXISTS appliances
(
    id             INT PRIMARY KEY,
    apartment_id   INT,
    name           VARCHAR(50) NOT NULL,
    appliance_type VARCHAR(50),
    FOREIGN KEY (apartment_id) REFERENCES apartments (id)
);

-- Table options
CREATE TABLE IF NOT EXISTS options
(
    id  INT PRIMARY KEY,
    name VARCHAR(50) NOT NULL
);

-- Table assoc_apartments_options
CREATE TABLE IF NOT EXISTS assoc_apartments_options
(
    apartment_id INT,
    option_id    INT,
    PRIMARY KEY (apartment_id, option_id),
    FOREIGN KEY (apartment_id) REFERENCES apartments (id),
    FOREIGN KEY (option_id) REFERENCES options (id)
);

-- Table common_facilities
CREATE TABLE IF NOT EXISTS common_facilities
(
    id                      INT PRIMARY KEY,
    name                    VARCHAR(50) NOT NULL,
    is_security_facility   BOOLEAN NOT NULL,
    last_verification_date DATE NULL
);

-- Table common_security_facilities
CREATE TABLE IF NOT EXISTS common_security_facilities
(
    id                 INT PRIMARY KEY,
    common_facility_id INT,
    FOREIGN KEY (common_facility_id) REFERENCES common_facilities (id)
);

-- Table assoc_building_common_facilities
CREATE TABLE IF NOT EXISTS assoc_building_common_facilities
(
    building_id             INT NOT NULL,
    common_facility_id      INT NOT NULL,
    last_verification_date DATE NULL,
    PRIMARY KEY (building_id, common_facility_id),
    FOREIGN KEY (building_id) REFERENCES buildings (id),
    FOREIGN KEY (common_facility_id) REFERENCES common_facilities (id)
);

-- Table tenants
CREATE TABLE IF NOT EXISTS tenants
(
    id           INT PRIMARY KEY,
    apartment_id INT NOT NULL,
    last_name    VARCHAR(50) NOT NULL,
    first_name   VARCHAR(50) NOT NULL,
    is_guarantor BOOLEAN NOT NULL,
    FOREIGN KEY (apartment_id) REFERENCES apartments (id)
);

-- Inserts for the apartment_types table
INSERT INTO apartment_types (id, type)
VALUES (1, 'T1'),
       (2, 'T2'),
       (3, 'T3');

-- Inserts for the buildings table
INSERT INTO buildings (id, name, address, date_construction)
VALUES (1, 'Immeuble A', '123 Rue de la Libération', '2024-03-07'),
       (2, 'Immeuble B', '456 Avenue des Acacias', '2024-03-07'),
       (3, 'Immeuble C', '789 Boulevard de la République', '2024-03-07');

-- Inserts for the apartments table
INSERT INTO apartments (id, number, area, building_id, apartment_type_id, max_occupants)
VALUES (1, 101, 75, 1, 1, 1),
       (2, 201, 90, 1, 2, 2),
       (3, 301, 80, 2, 3, 3),
       (4, 102, 65, 2, 1, 1),
       (5, 202, 70, 3, 2, 2),
       (6, 302, 85, 3, 3, 3);

-- Inserts for the appliances table
INSERT INTO appliances (id, name, appliance_type, apartment_id)
VALUES (1, 'Réfrigérateur', 'Electroménager', 1),
       (2, 'Cuisinière', 'Electroménager', 1),
       (3, 'Télévision', 'Electronique', 2),
       (4, 'Lave-linge', 'Electroménager', 3),
       (5, 'Four micro-ondes', 'Electroménager', 4),
       (6, 'Climatiseur', 'Climatisation', 5),
       (7, 'Aspirateur', 'Electroménager', 6),
       (8, 'Ordinateur portable', 'Informatique', 2),
       (9, 'Lampadaire', 'Eclairage', 3),
       (10, 'Sèche-cheveux', 'Electroménager', 1),
       (11, 'Machine à café', 'Electroménager', 5),
       (12, 'Ventilateur', 'Ventilation', 3),
       (13, 'Fer à repasser', 'Electroménager', 4),
       (14, 'Chauffe-eau', 'Chauffage', 2),
       (15, 'Téléphone portable', 'Electronique', 6),
       (16, 'Lave-vaisselle', 'Electroménager', 4),
       (17, 'Radio', 'Electronique', 1),
       (18, 'Console de jeux', 'Electronique', 5),
       (19, 'Imprimante', 'Informatique', 3),
       (20, 'Lampe de bureau', 'Eclairage', 2);

-- Inserts for the options table
INSERT INTO options (id, name)
VALUES (1, 'Parking'),
       (2, 'Piscine'),
       (3, 'Gym');

-- Inserts for the assoc_apartments_options table
INSERT INTO assoc_apartments_options (apartment_id, option_id)
VALUES (1, 1),
       (2, 2),
       (3, 3),
       (4, 1),
       (5, 2),
       (6, 3);

-- Inserts for the common_facilities table
INSERT INTO common_facilities (id, name, is_security_facility, last_verification_date)
VALUES (1, 'Salle de réunion', TRUE, '2024-03-07'),
       (2, 'Salle de sport', FALSE, NULL),
       (3, 'Espace détente', TRUE, '2024-03-07');

-- Inserts for the common_security_facilities table
INSERT INTO common_security_facilities (id, common_facility_id)
VALUES (1, 1),
       (2, 2),
       (3, 3);

-- Inserts for the assoc_building_common_facilities tabledata
INSERT INTO assoc_building_common_facilities (building_id, common_facility_id, last_verification_date)
VALUES (1, 1, '2024-03-07'),
       (1, 2, '2024-03-07'),
       (2, 2, '2024-03-07'),
       (2, 3, '2024-03-07'),
       (3, 1, '2024-03-07'),
       (3, 3, '2024-03-07');

-- Inserts for the tenants table
INSERT INTO tenants (id, apartment_id, last_name, first_name, is_guarantor)
VALUES (1, 1, 'Dupont', 'Jean', FALSE),
       (2, 1, 'Martin', 'Sophie', TRUE),
       (3, 1, 'Dubois', 'Pierre', FALSE),
       (4, 2, 'Lefevre', 'Marie', FALSE),
       (5, 2, 'Leclerc', 'Paul', FALSE),
       (6, 2, 'Durand', 'Julie', TRUE),
       (7, 3, 'Girard', 'Luc', TRUE),
       (8, 3, 'Moreau', 'Isabelle', FALSE),
       (9, 3, 'Roux', 'Thomas', FALSE);
